## vim:set et ts=4 sw=4:
#
# proposed.py: parse the comment files of the proposed-updates-NEW queue
#
# (C) 2007-2008 Philipp Kern <pkern@debian.org>
# (C) 2009-2017 Adam D. Barratt <adam@adam-barratt.org.uk>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2, or (at your option)
# any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301  USA.
#
# Dependencies (expressed in Debian packages):
#  * python-apt (for version comparisons)
#  * python-debian (>= 0.1.21+nmu3)

import errno
import logging
import os
import re
import stat
import sys
from io import open
from collections import defaultdict
from datetime import datetime, timezone
from functools import cmp_to_key

import apt_pkg

from debian.deb822 import Deb822

from debrelease import extract_epoch, normalize_path, strip_epoch, IGNORABLE_TIME_DELTA, tree
from debrelease.security import DSAFactory

apt_pkg.init_system()

class QueueParser(object):
    cve_re = re.compile(r"CVE-\d{4}-\d+")

    def __init__(self, projectb, config, section):
        """Queue parser initialisation

        :param projectb: a ProjectB object

        :param config: a ConfigParser object

        :param section: the section within the config object to look for
            all needed information"""

        self.logger = logging.getLogger('debrelease.proposed')
        self.logger.addHandler(logging.NullHandler())
        self.logger.debug('QueueParser: __init__')
        self.projectb = projectb
        self.suite = section
        self.base_suite = config.get(section, 'base_suite')
        self.above_suite = config.get(section, 'above_suite')
        self.policy_suite = config.get(section, 'policy_suite')
        self.codename = config.get(section, 'codename')
        self.release_architectures = \
            set(config.get(section, 'release_architectures').split(' '))
        mappings = config.get(section, 'package_mappings', fallback='')
        if mappings:
            self.package_mappings = \
                {package: mapped_packages.split(',') \
                      for (package, mapped_packages) in (mapping.split('=') \
                      for mapping in mappings.split(';'))}
        else:
            self.package_mappings = {}

        self.dsafactory = DSAFactory(config, section)

        self.todos = []
        self.removals = {}

        self.vercmp = cmp_to_key(apt_pkg.version_compare)

        self.directory = normalize_path(config.get(section, 'directory'))
        self.comments_directory = \
            normalize_path(config.get(section, 'comments_directory'))
        self.entries = defaultdict(dict)
        self._parse_changes()
        self._parse_comments()
        self._parse_uploads()
        self._associate_security_updates()
        self._parse_todos()
        self._parse_removals()

    def _parse_comments(self):
        """Parses the comments directory within the queue to determine if
        packages should be accepted or rejected by the archive scripts (dak).
        The filenames adhere to a scheme and this function depends on it being
        applied throughout:

            Packages which are not ready for a specific action:
                package_version
            Packages to be accepted by dak:
                ACCEPT.package_version
            Packages to be rejected by dak:
                REJECT.package_version
            Packages already accepted by dak and kept for reference:
                ACCEPTED.package_version
            Packages already rejected by dak and kept for reference:
                REJECTED.package_version

        All files in the directory which start with a dot are ignored, thus
        swap files should not impose a problem.

        The file content needs to adhere to the following specification:
            line 1:    NOTOK|OK|UNKNOWN|MOREINFO
            line 2:    [DSA <number> ]<source pkg> - <reason>
            line 3..n: [additional comments]

        Line 1 determines if the Release Managers have concerns related to the
        inclusion of the package.  It does not imply an action.  To be
        considered the files need to be renamed to adhere to the specification
        outlined above.  The content of the comments file is thus purely
        informal.

        This function is used internally by __init__."""

        #
        get_architectures = self.projectb.get_architectures
        get_binaries = self.projectb.get_binaries
        get_binary_filename = self.projectb.get_binary_filename
        get_binary_version = self.projectb.get_binary_version
        get_version = self.projectb.get_version

        self.logger.debug('QueueParser: _parse_comments()')
        actionable = re.compile(r"((ACCEPT|REJECT)(|ED))\.(.*)")
        comment_files = os.listdir(self.comments_directory)
        self.logger.info('Reading comment files')
        for comment_filename in sorted(comment_files):
            filename = "%s/%s" % (self.comments_directory, comment_filename)
            # Ignore all directories below the comments directory.
            try:
                if stat.S_ISDIR(os.stat(filename).st_mode):
                    continue
            except OSError as inst:
                if inst.errno == errno.ENOENT:
                    # File disappeared under us. It may have been renamed
                    # or removed, but in either case we can ignore it.
                    continue
                raise
            # Ignore all files starting with a dot (e.g. swap files)
            if re.match(r'^\.', comment_filename):
                continue
            # Ignore special TODO and REMOVALS files, handled seperately.
            if comment_filename in ['TODO', 'REMOVALS']:
                continue
            # Ignore special TEMPLATE file, used by scripts.
            if comment_filename in ['TEMPLATE']:
                continue

            self.logger.info("Considering %s", comment_filename)

            # Retrieve information from the filename
            try:
                header, version = comment_filename.rsplit('_', 1)
            except ValueError:
                sys.stderr.write("WARNING: comments file %s has malformed "
                                 "filename, ignoring.\n" % comment_filename)
                sys.stderr.flush()
                continue

            version = strip_epoch(version)
            binNMU = False
            match = re.match(actionable, header)
            if match:
                # Use the actionable regexp to determine if an action was
                # specified in the filename.  Simple checking for the dot
                # in it fails on source packages with dots in their name.
                action = match.group(1)
                package = match.group(4)
            else:
                # Without an action it is undetermined for the ftp-masters what
                # to do with a specific package, even if it is ready according
                # to the contents of the comments file.
                action = 'UNKNOWN'
                package = header
            action = action.upper()

            if package in self.entries and version in self.entries[package] and \
                  'seen' in self.entries[package][version]:
                seen = self.entries[package][version]['seen']
                upload_age = (datetime.now(timezone.utc) - seen).total_seconds() / 60
            else:
                upload_age = 0

            # When the action is ACCEPTED or REJECTED we expect no changes file
            # to be present, i.e. there won't be an entry in the hashmap about
            # the upload.
            if action == 'ACCEPTED' or action == 'REJECTED':
                # Check for the version entry in the hashmap.  If it exists
                # display a warning because the changes file is still in the
                # queue directory albeit the upload was already processed.
                # Then subsequently overwrite the entry in the hashmap.
                if version in self.entries[package] and (upload_age > IGNORABLE_TIME_DELTA or upload_age == 0):
                    sys.stderr.write("WARNING: comments file %s suggests that "
                                     "the upload is already accepted/rejected but changes "
                                     "file is still in the queue directory, ignoring the "
                                     "latter.\n" % comment_filename)
                    sys.stderr.flush()
                self.entries[package][version] = {}

            base_version = get_version(self.base_suite, package, epoch=True)
            proposed_version = get_version(self.suite, package, epoch=True)

            # Resiliency for missing changes files
            try:
                entry = self.entries[package][version]
            except KeyError:
                if action == 'ACCEPT' and version == strip_epoch(proposed_version):
                    action = "ACCEPTED"
                    self.entries[package][version] = {}
                    entry = self.entries[package][version]
                else:
                    sys.stderr.write("WARNING: comments file %s found without "
                                     "corresponding changes file, ignoring.\n" % comment_filename)
                    sys.stderr.flush()
                    continue

            entry['upload_age'] = upload_age

            if action == 'ACCEPTED':
                # Check if we look at the most current version in p-u or at
                # an older build previously accepted but now superseded.
                if proposed_version is not None:
                    entry['proposed_version'] = strip_epoch(proposed_version)
                    entry['diff_against_overlay'] = (version != entry['proposed_version'])
                    epoch = extract_epoch(proposed_version)
                    if epoch is not None:
                        # Prepend the epoch of the version in p-u.
                        # This will always be correct if we're looking at that
                        # version; if we're not then we assume that the epochs
                        # will have been the same.
                        entry['fullversion'] = "%s%s" % (epoch, version)
                    else:
                        entry['fullversion'] = version
                    version_comp = apt_pkg.version_compare(entry['proposed_version'],
                                                           version)
                    if version_comp > 0:
                        entry['superseded'] = True
                    elif version_comp < 0:
                        binNMU = True
                else:
                    # Seems we are missing the source.  Now if the binary is
                    # epoched our version checks will be wrong, if we just
                    # take the version infered from the filename.  We know
                    # that the source is in the base suite, though, so let's
                    # take its version instead.
                    binNMU = True
                    entry['diff_against_overlay'] = False
                    entry['fullversion'] = base_version

                if binNMU:
                    entry['binNMU'] = True

                # Retrieve list of installed architectures by querying
                # projectb.  As there may be multiple versions in the suite
                # we need to specify exactly which version we mean.
                installed = get_architectures(self.suite,
                                              package, version,
                                              binNMU)
                # Retrieve list of builds currently in base_suite, through
                # projectb.
                released = get_architectures(self.base_suite, package)
                entry['architectures'] = installed
                # BinNMUs will by definition not include all architectures
                # so don't moan about them being missing
                if not binNMU:
                    # We expect to have builds for all release architectures
                    # for which the package was built in the base suite
                    entry['missing_builds'] = (released - installed) & self.release_architectures
                entry['binaries'] = tree()
                binaries = entry['binaries']
                for arch in released:
                    # Get a list of package versions for binaries which are
                    # built from this source.
                    binpkgs = get_binaries(self.suite, package, arch, entry['fullversion'])
                    binpkgs |= get_binaries(self.base_suite, package, arch)
                    for binary in binpkgs:
                        bin_version = get_binary_version(self.suite, binary, arch, epoch=True)
                        base_bin_version = get_binary_version(self.base_suite, binary, arch, epoch=True)
                        bininfo = binaries[arch][binary]
                        if bin_version is not None:
                            bin_filename = get_binary_filename(self.suite, package, binary, arch)
                            bininfo['proposed'] = (bin_version, bin_filename)
                        if base_bin_version is not None:
                            bin_filename = get_binary_filename(self.base_suite, package, binary, arch)
                            bininfo['base'] = (base_bin_version, bin_filename)

            if action != 'UNKNOWN':
                entry['action'] = action

            self.process_comment_file(comment_filename, entry, package, version)
            if package not in self.entries or version not in self.entries[package]:
                # if comment parsing fails fatally
                continue

            # "Bring architectures back in sync" uploads do not contain all
            # architectures.  So if version(base_suite) == version(suite)
            # do not check for missing builds and clear the superseded flag.
            above_version = get_version(self.above_suite, package, epoch=True)
            if above_version is not None:
                entry['above_fullversion'] = above_version
                entry['above_version'] = strip_epoch(above_version)
            if base_version is None:
                entry['new_source'] = True
                continue
            entry['base_fullversion'] = base_version
            entry['base_version'] = strip_epoch(base_version)
            if entry['base_version'] != version:
                continue
            if 'missing_builds' in entry and 'expect_arches' not in entry:
                del entry['missing_builds']
            if 'superseded' in entry:
                del entry['superseded']

    def process_comment_file(self, comment_filename, entry, package, version):
        # Read the contents for the comments file, see the docstring for the
        # specification
        filename = "%s/%s" % (self.comments_directory, comment_filename)
        self.logger.debug('QueueParser: _process_comment_file')
        self.logger.debug("Considering %s", filename)
        with open(filename, encoding='utf-8') as comments:
            lines = list(comments)

        # line 1
        entry['status'] = lines[0].strip()
        # line 2
        # [DSA <number> ]<pkg> - <reason>
        try:
            header, reason = lines[1].split(' - ', 1)
        except ValueError:
            sys.stderr.write("WARNING: comments file %s line 2 "
                             "malformed, continuing somehow.\n" % comment_filename)
            sys.stderr.flush()
            header, reason = '', lines[1]
        except IndexError:
            sys.stderr.write("WARNING: comments file %s does not "
                             "contain at least two lines; ignoring.\n" % comment_filename)
            sys.stderr.flush()
            del self.entries[package][version]
            if not self.entries[package]:
                del self.entries[package]
            return
        entry['reason'] = reason.strip()
        if 'cves' not in entry:
            entry['cves'] = set()
        entry['cves'].update(re.findall(self.cve_re, reason))
        # [<other comments>]
        # (signature delimiter "-- \n")
        # [<non-displayed comments>]
        if len(lines) > 2:
            entry['comments'] = []
            found_delim = False
            for line in lines[2:]:
                line = line.rstrip("\n")
                if line == '-- ':
                    # Do not output any further lines, including the current one
                    # They may however still contain useful metadata
                    found_delim = True
                if found_delim:
                    if ':' in line:
                        key, value = line.split(':', 1)
                        key = key.replace('-', '_')
                        entry[key] = value.strip()
                else:
                    entry['comments'].append(line)
                    entry['cves'].update(re.findall(self.cve_re, line))
        released_arches = []
        m = re.search(r'DSA (\d+)', header)
        if m:
            # We are dealing with a security update.
            entry['advisory'] = m.group(1)
            # As the package may only enter ACCEPTED when all builds are
            # received, we should display a warning and a list of missing
            # binaries.
            if 'architectures' in entry and 'binNMU' not in entry:
                released_arches = self.projectb.get_architectures(self.base_suite, package)
                for mapped_package in self.package_mappings.get(package, []):
                    released_arches |= self.projectb.get_architectures(self.base_suite, mapped_package)
        elif 'binNMU' in entry and 'expect_arches' in entry and 'architectures' in entry:
            released_arches = set(entry['expect_arches'].split(' '))
        if released_arches:
            closed_arches = entry['architectures']
            open_arches = (released_arches - closed_arches) & self.release_architectures
            if open_arches:
                entry['missing_builds'] = open_arches

    def _associate_security_updates(self):
        """Looks at entries without a comment file and creates them if it finds
        information about them in the security database."""

        self.logger.info('QueueParser: _associate_security_updates')

        for package in self.entries:
            for version in self.entries[package]:
                entry = self.entries[package][version]
                if 'status' in entry:
                    # Comment file already present.
                    continue
                info = self.dsafactory.get_dsa(package,
                                               entry.get('fullversion', version))
                if not info:
                    # Not a security update.
                    continue
                entry['status'] = 'UNKNOWN'
                entry['advisory'] = info['advisory'].split('-')[1]
                entry['reason'] = info['description']
                filename = os.path.join(self.comments_directory,
                                        '%s_%s' % (package, version))
                self.logger.info("Creating security comment for %s_%s", package, version)
                with open(filename, 'w') as f:
                    f.write("UNKNOWN\n")
                    f.write("DSA %s %s - %s\n" % (entry['advisory'],
                                                  package,
                                                  entry['reason']))

    def _parse_changes(self):
        """Open the queue directory and parse the changes files therein
        to retrieve the available versions and architectures available of the
        packages in the queue.  Information in the filename is not used.

        This function is used internally by __init__."""

        self.logger.info('QueueParser: _parse_changes')

        files = self.projectb.get_changes_files_for_policy_queue(self.policy_suite)
        for changesfile, changesid in sorted(files):
            # Extract the name of the source package, the package's version and
            # the available architectures from the changes files.  Do not rely
            # on the filename in any way.
            self.logger.debug(".changes file ID %s", changesid)
            changes = self.projectb.get_changes_details(changesid)
            seen = changes.seen
            package, fullversion, binNMU = (
                changes.source, changes.version,
                self._is_binNMU(changes.source))
            version = strip_epoch(fullversion)
            if binNMU:
                # Normalise the source package name, which has the original
                # source version appended in brackets.
                package, source_version = self._split_binNMU(package)
            self.logger.info("Considering %s", changesfile)
            architectures = set(changes.architecture.split(' '))
            if package in self.entries:
                if version not in self.entries[package]:
                    self.entries[package][version] = {}
                    entry = self.entries[package][version]
                    entry['architectures'] = architectures
                    entry['fullversion'] = fullversion
                    entry['seen'] = datetime.min.replace(tzinfo=timezone.utc)
                else:
                    entry = self.entries[package][version]
                    # If subsequent changes files for specific versions are
                    # encountered, then add their architectures to the list.
                    entry['architectures'] |= architectures
            else:
                self.entries[package][version] = {'architectures': architectures,
                                                  'seen': seen,
                                                  'fullversion': fullversion}
                entry = self.entries[package][version]
            # record the "seen" timestamps of the .changes files for possible
            # checking later
            entry['seen'] = max(seen, entry['seen'])

            files = self.projectb.get_binaries_for_policy_queue_by_changes(changesid)
            for binpkg, bin_version, arch, binary in files:
                if 'binaries' not in entry:
                    entry['binaries'] = tree()
                bininfo = entry['binaries'][arch][binpkg]
                base_bin_version = self.projectb.get_binary_version(
                                        self.base_suite, binpkg, arch, epoch=True)

                bininfo['proposed'] = (bin_version, binary)
                overlay_bin_version = self.projectb.get_binary_version(
                                          self.suite, binpkg, arch, epoch=True)
                if overlay_bin_version is not None:
                    bin_filename = self.projectb.get_binary_filename(
                                       self.suite, package, binpkg, arch)
                    bininfo['overlay'] = (overlay_bin_version, bin_filename)

                if base_bin_version is not None:
                    bin_filename = self.projectb.get_binary_filename(
                                       self.base_suite, package, binpkg, arch)
                    bininfo['base'] = (base_bin_version, bin_filename)

            if binNMU:
                entry['binNMU'] = True
                entry['source_version'] = source_version
            above_version = \
                         self.projectb.get_version(self.above_suite, package, epoch=True)
            if above_version is not None:
                entry['above_fullversion'] = above_version
                entry['above_version'] = strip_epoch(above_version)
            base_version = \
                         self.projectb.get_version(self.base_suite, package, epoch=True)
            if base_version is not None:
                entry['base_fullversion'] = base_version
                entry['base_version'] = strip_epoch(base_version)
            proposed_version = self.projectb.get_version(self.suite, package, epoch=True)
            if proposed_version is not None:
                entry['proposed_version'] = strip_epoch(proposed_version)
                entry['diff_against_overlay'] = (version != entry['proposed_version'])
            else:
                entry['diff_against_overlay'] = False

    def _parse_uploads(self):
        self.logger.info('QueueParser: _parse_uploads')
        for package in sorted(self.entries):
            for version in sorted(self.entries[package], key=self.vercmp):
                self.logger.info("Considering %s_%s", package, version)
                entry = self.entries[package][version]
                fullversion = entry.get('fullversion', version)
                entry['bugs_closed'] = \
                    self.projectb.get_bugs_closed(package, fullversion)
                if 'cves' not in entry:
                    entry['cves'] = set()
                entry['cves'].update(re.findall(self.cve_re,
                       self.projectb.get_changelog(package,
                       fullversion)))
                has_udebs = self.projectb.has_udebs(self.base_suite, package)
                if has_udebs:
                    udeb_only = self.projectb.is_udeb_only(self.base_suite, package)
                else:
                    udeb_only = False
                entry['has_udebs'] = has_udebs
                entry['udeb_only'] = udeb_only

                if 'binaries' in entry:
                    for arch in entry['binaries']:
                        for binpkg in entry['binaries'][arch]:
                            bininfo = entry['binaries'][arch][binpkg]
                            above_bin_version = self.projectb.get_binary_version(
                                                    self.above_suite, binpkg, arch, epoch=True)
                            if above_bin_version is not None:
                                bin_filename = self.projectb.get_binary_filename(
                                                   self.above_suite, package, binpkg, arch)
                                bininfo['above'] = (above_bin_version, bin_filename)

    binNMU_regexp = re.compile(r'(.+) \((.+)\)')

    def _is_binNMU(self, source):
        return re.match(self.binNMU_regexp, source) is not None

    def _split_binNMU(self, source):
        return re.match(self.binNMU_regexp, source).groups()

    def _parse_todos(self):
        """Checks for the existence of a TODO file in the comment directory.
        Every line in it creates a new bullet in the output."""
        self.logger.debug('QueueParser: _parse_todos')
        filename = os.path.join(self.comments_directory, 'TODO')
        if os.path.exists(filename):
            with open(filename, encoding='utf-8') as todo_file:
                self.todos = [x.strip() for x in todo_file if len(x.strip())]

    def _parse_removals(self):
        """Checks for the existence of a REMOVALS file in the comments
        directory.  Every line in it needs to adhere to the following
        format:
          #<bug number>: <source pkg> - <reason>"""
        self.logger.debug('QueueParser: _parse_removals')
        filename = os.path.join(self.comments_directory, 'REMOVALS')
        if os.path.exists(filename):
            with open(filename, encoding='utf-8') as removals_file:
                for line in removals_file:
                    bug, tail = line.strip().split(':', 1)
                    srcpkg, reason = tail.strip().split(' - ')
                    self.removals[srcpkg] = (bug[1:], reason)
