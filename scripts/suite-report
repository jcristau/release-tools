#! /bin/sh
#
# suite-report: produce reports about the content of Debian archive suites
#
# (C) Copyright 2018 Adam D. Barratt <adsb@debian.org>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2, or (at your option)
# any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301  USA.

new_for_suite() {
	local SUITE=$1

	echo "
		SELECT
			changes.source \"Source\",
			changes.version \"Version\",
			changes.distribution \"Distribution\",
			uid.name || ' <' || uid.uid || '>' \"Uploader\",
			changes.changedby \"Changed By\",
			-- Remove the first two lines of the changelog,
			-- remove leading indents from the changelog,
			-- and return at most 20 lines
			REGEXP_REPLACE(REGEXP_REPLACE(
				REGEXP_REPLACE(changelogs.changelog, '^(.*?\n){2}', ''),
				'^   ', '', 'gn'
			), '^((?:.*?\n){,20}).*', '\1') \"Changelog\"
		FROM
			suite
			INNER JOIN policy_queue_upload u ON target_suite_id=suite.id
			INNER JOIN changes ON u.changes_id=changes.id
			INNER JOIN fingerprint fpr ON changes.fingerprint=fpr.fingerprint
			INNER JOIN uid ON fpr.uid=uid.id
			INNER JOIN changelogs ON changelogs.id=changes.changelog_id
		WHERE
			u.policy_queue_id=4
			AND
			suite_name='$SUITE'" \
	| psql -xt service=projectb \
	| sed -e "s/+$//" -e "s/^\( *\)|/\1 /" \
	      -e "/^ *\. *$/d" \
	      -e "s/^-*+-*//g" \
	      -e "/^[^ ]/{s/\( *\)|/:\1/}"
}

check_suite() {
	local SUITE=$1

	printf "Report for $SUITE\n==========="
	printf "%*s\n\n" ${#SUITE} | tr ' ' '='
	printf "Generated: $(date "+%Y-%m-%d %H:%M")\n\n"

	NEW=$(new_for_suite $SUITE)
	if [ -n "$NEW" ]
	then
		printf "The following packages in the NEW queue are destined for $SUITE:\n\n"
		echo "$NEW"
	else
		printf "There are no packages in the NEW queue destined for $SUITE\n\n"
	fi
}

mail_suite_status() {
	local SUITE=$1

	check_suite $1 | mail -a "X-Debian: release-tools suite-report" -r "Debian Release Team <debian-release@lists.debian.org>" -s "Suite status report for $SUITE" debian-release@lists.debian.org
}

for SUITE in proposed-updates # oldstable-proposed-updates
do
	mail_suite_status $SUITE
done
