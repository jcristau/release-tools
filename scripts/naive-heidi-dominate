#!/usr/bin/python3
# vim:set et ts=4 sw=4 cindent:
from __future__ import print_function

from debian.debian_support import Version
from collections import defaultdict
from optparse import OptionParser
import sys


def defdict():
    return defaultdict(list)

def dominate(f):
    r'''
    >>> dominate(["cmake 2.8.2-2 all\n",
    ...           "cmake 2.8.2+dfsg.1-0+squeeze1 all\n"])
    cmake 2.8.2+dfsg.1-0+squeeze1 all
    '''
    packages = defaultdict(defdict)
    for line in f:
        package, version, architecture = line.strip().split(' ')
        if package in packages and architecture in packages[package]:
            if Version(version) < Version(packages[package][architecture]):
                continue
        packages[package][architecture] = version
    for package in packages:
        for architecture in packages[package]:
            print('%s %s %s' % (package, packages[package][architecture], architecture))

def main():
    parser = OptionParser()
    parser.add_option('-t', '--test', action='store_true')
    parser.add_option('-f', '--file', metavar='FILE')
    (options, args) = parser.parse_args()

    if options.test:
        import doctest
        failure_count, test_count = doctest.testmod()
        if failure_count > 0:
            return 1
        else:
            return 0

    if not options.file:
        dominate(sys.stdin)
    else:
        with open(options.file, 'r') as f:
            dominate(f)
    return 0

if __name__ == '__main__':
    sys.exit(main())
