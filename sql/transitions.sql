CREATE TABLE transitions (
    source text
      NOT NULL
      PRIMARY KEY,
    oldpkg text
      NOT NULL,
    newpkg text
      NOT NULL
);

-- Transitions that are registered but
-- have not been uploaded to unstable yet.
CREATE VIEW unstarted_transitions AS
    SELECT DISTINCT source
    FROM transitions
    WHERE newpkg NOT IN (
      SELECT package FROM suite_binaries
        WHERE suite = 'unstable');

-- Registered transitions that finished already
-- (the old package is not in testing)
CREATE VIEW finished_transitions AS
    SELECT DISTINCT source
    FROM transitions
    WHERE oldpkg NOT IN (
      SELECT package FROM suite_binaries
        WHERE suite = 'testing');

-- Transition that are in progress: new package in
-- unstable already but not in testing yet
CREATE VIEW ongoing_transitions AS
    SELECT DISTINCT source
    FROM transitions
    WHERE newpkg NOT IN (
      SELECT package FROM suite_binaries
        WHERE suite = 'testing')
    AND newpkg IN (
      SELECT package FROM suite_binaries
        WHERE suite = 'unstable');

-- Transitions with old binaries in testing
CREATE VIEW partial_transitions AS
    SELECT DISTINCT source
    FROM transitions
    WHERE oldpkg IN (
      SELECT package FROM suite_binaries
        WHERE suite = 'testing')
    AND newpkg IN (
      SELECT package FROM suite_binaries
        WHERE suite = 'testing');
