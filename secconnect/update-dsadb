#!/usr/bin/python
# vim:set et sw=4 ft=python:

import os
import stat
import sys
import warnings
from six.moves import cPickle

SECURE_TESTING_ROOT = os.path.join(os.environ['HOME'], 'var/data/security-tracker')
DSA_LIST = os.path.join(SECURE_TESTING_ROOT, 'data/DSA/list')
SUITES = ['stretch', 'jessie']
DSA_DB = os.path.join(os.environ['HOME'], 'var/data/dsa.db')
UPD_FLAG = os.path.join(SECURE_TESTING_ROOT, 'data/updated')

def main():
    needs_rebuild =    not os.path.exists(DSA_DB) \
                    or os.stat(DSA_DB)[stat.ST_SIZE] == 0 \
                    or os.stat(DSA_DB)[stat.ST_MTIME] \
                       < os.stat(UPD_FLAG)[stat.ST_MTIME]
    if not needs_rebuild:
        return

    # Import secure-testing libraries
    sys.path.append(os.path.join(SECURE_TESTING_ROOT, 'lib/python'))
    import debian_support
    import bugs

    data = bugs.DSAFile(DSA_LIST, file(DSA_LIST))

    dsa_list = dict()
    errors = False
    try:
        for record in data:
            for note in record.notes:
                if str(note.release) not in SUITES:
                    continue
                dsa, description = str(record.name), str(record.description)
                date = str(record.date)
                srcpkgs, version = str(note.package).split(' '), str(note.fixed_version)
                for srcpkg in srcpkgs:
                    if srcpkg not in dsa_list:
                        dsa_list[srcpkg] = {}
                    dsa_list[srcpkg][version] = {'advisory': dsa,
                                                 'description': description,
                                                 'date': date}
    except debian_support.ParseError, e:
        e.printOut(sys.stderr)
        errors = True

    # Do not update the database if we found syntax errors.
    if errors:
        sys.exit(1)

    # Dump the parsed data into a file for quick re-access.
    cPickle.dump(dsa_list, open(DSA_DB, 'w'), cPickle.HIGHEST_PROTOCOL)

if __name__ == '__main__':
    main()
