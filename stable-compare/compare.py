#!/usr/bin/env python
import apt_pkg, gzip, pickle, time, sys, os

apt_pkg.init_config()
apt_pkg.init_system()

now = time.time()
suite = len(sys.argv) > 1 and sys.argv[1] or "stable"

basedir = '/srv/release.debian.org'
tmpdir = '%s/tmp' % (basedir)
template_filename = '%s/template' % (os.path.dirname(__file__))
output_filename = '%s/www/proposed-updates/missing-security-%s.html' % (basedir, suite)

# Parse = apt_pkg.ParseTagFile(gzip.open("/srv/release.debian.org/data/stable-security/main/binary-i386/Packages.gz","r"));

def parse(file, arch=None, ret = None):
    if ret is None:
        ret = {}
    with open(file) as f:
        Parse = apt_pkg.TagFile(f)
    step = Parse.step
    section = Parse.section
    while step() == 1:
        p = section.get("Package")
        s = section.get("Source")
        if s:
            if s.find(' (') != -1:
                p=s[:s.find(' ')]
            else:
                p=s
        a = arch or section.get("Architecture")
        ret.setdefault(p, {})
        if not a in ret[p] or apt_pkg.version_compare(section.get("Version"), ret[p][a]) > 0:
            ret[p][a]=section.get("Version")
    return ret

sec=parse('%s/%s-Packages-security' % (tmpdir, suite))
sec=parse('%s/%s-Sources-security' % (tmpdir, suite), 'source', sec)
prop=parse('%s/%s-Packages-proposed' % (tmpdir, suite))
prop=parse('%s/%s-Sources-proposed' % (tmpdir, suite), 'source', prop)
picklefile = '%s/missing-%s' % (tmpdir, suite)

def check(sec, prop):
    global missing
    result = {}
    for i in sec:
        for a in sec[i]:
            if not i in prop or not a in prop[i] or apt_pkg.version_compare(sec[i][a], prop[i][a]) > 0:
                stamp = "%s/%s" % (i, a)
                prop.setdefault(i,{})
                prop[i].setdefault(a,"n/a")
                if not stamp in missing: missing[stamp] = now
                if now - missing[stamp] > 36 * 3600:
                    c = "color=\"red\""
                else:
                    c = ""
                key = '%s/%s' % (i, '_source' if a == 'source' else a)
                result[key] = (i, a)
    return result

# missing = pickle.load(open(mapsuitetofiles[suite]['pickle'],"r"))
missing = {}
results = check(sec, prop)
with open(picklefile, 'wb') as out:
    pickle.dump(missing, out, -1)

missing_packages = []
for key in sorted(results):
    (package, arch) = results[key]
    missing_packages.append( "<li>%s/%s (%s vs %s)</li>" % (package, arch,
      sec[package][arch],
      prop[package][arch]
    ))
if missing_packages:
    missing_packages.insert(0, '<ul>')
    missing_packages.append('</ul>')

with open(template_filename, 'r') as template_file:
    template = template_file.read()

template = template.replace('$SUITE$', suite)
template = template.replace('$LIST$', '\n'.join(missing_packages))
template = template.replace('$TIME$', time.strftime("%Y-%m-%dT%H:%M:%SZ", time.gmtime()))

with open(output_filename, 'w') as output_file:
    output_file.write(template)
