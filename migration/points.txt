<h3>Score algorithm:</h3>
<ol>
<li> Start with the number of installed instances reported by <a href="http://popcon.debian.org">popcon.debian.org</a> + 1
<li> Add the number of installed instances for all packages stalled by it
<li> Multiply with 1.5 if there are open RC bugs
</ol>
<p>Faultless packages in their grace period are not listed. Neither are RC-bug free frozen ones. See html source for per-package details.
<p>
