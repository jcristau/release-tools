#!/usr/bin/perl
#
# Extracts info from update_excuses.html[1] and update_output.txt[2] and
# attempts to answer the question "why is package X not in testing yet?"
#
# This script is published under the GNU General Public License version 2.0.
# See http://bjorn.haxx.se/debian/COPYING for full license details.
#
# Also see 'cronjob.txt' in this directory for the data fetching.
#
# Bj�rn Stenberg <bjorn@haxx.se>
#
# References:
# [1] https://release.debian.org/britney/update_excuses.html
# [2] https://release.debian.org/britney/update_output.txt
# [3] http://incoming.debian.org/
# [4] http://popcon.debian.org/by_inst
#

use CGI 'param';
use URI::Escape;
use POSIX 'strftime';
use HTTP::Date;

# initialise libapt-pkg-perl for use in vercmp
use AptPkg::Config '$_config';
use AptPkg::System '$_system';
use AptPkg::Version;
$_config->init;
$_system = $_config->system;

my $BASE_DIR = '/srv/release.debian.org/tools/migration';
my $OUT_DIR = '/srv/release.debian.org/www/migration';

my $green = "#006000";
my $startgreen = "<font color=\"$green\">";
my $stopgreen = "</font>";
$printalldeps = 0;

$cginame = "testing.pl";
$excuses = "https://release.debian.org/britney/update_excuses.html";

# calculate Last-Modified header to allow caching
@files = ( "$OUT_DIR/packages-main",
           "$OUT_DIR/sources-main",
           "$OUT_DIR/sources-contrib",
           "$OUT_DIR/sources-non-free");
$toptime = 0;
for (@files) {
    $mtime = (stat($_))[9];
    $toptime = $mtime if ($mtime > $toptime);
}
$cgitime = (stat($cginame))[9];

@times = gmtime $toptime;
$lastmodified = strftime "%a, %d %b %Y %T GMT", @times;

if (defined $ENV{HTTP_IF_MODIFIED_SINCE}) {
    my $ims = $ENV{HTTP_IF_MODIFIED_SINCE};
    my $imstime = HTTP::Date::str2time($ims);
    if ($imstime >= $toptime and $imstime >= $cgitime) {
        print "Status: 304 Not Modified\n\n";
        exit;
    }
}

if (1 and scalar @ARGV) {
    &readfiles(1);
    &handle_argv();
    exit;
}

$printalldeps = param("printalldeps") + 0;
$expand = param("expand") + 0;
$only_testing = param("nonew") + 0;
$only_ready = param("ready") + 0;

chdir($OUT_DIR);
print "Last-Modified: $lastmodified\n";
print "Content-type: text/html; charset=iso-8859-1\n\n";

my $n = param("package");
$n =~ /(^[a-zA-Z0-9\-\.\+]+)/; # ignore silly characters
my $name = $1;

if ($ENV{QUERY_STRING} eq "") {
    $name = "_index";
}

if (0 and scalar @ARGV) {
    $name = shift @ARGV;
}

if (!$printalldeps and !param(waiting) and !param(staller) and !param(nocache)) {
    $name =~ /^(.)/;
    my $dir = $1;
    my $filename = sprintf("cache/$dir/$name%s.html", $expand ? ".$expand" : "");
    if ($name eq "_index") {
        $filename = "cache/_index.html";
	}
    if (open CACHED, "<$filename") {
        print <CACHED>;
        close CACHED;
        exit;
    }
}
&readfiles();
print &mkpage($name);
exit;

sub mkpage {
    my $name = shift @_;
    my $out;

    $depth = $bdepth = $broken = $recursed = 0;
    %explained = %bexplained = ();
    %reportedbreak = ();
    %done = ();
    $topname = $newtopname = "";

    my $title = "Why is package X not in testing yet?";
    if ($name ne "") {
        $title = "$name - $title";
    }

    $out .= '<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"';
    $out .= "\n" . '    "http://www.w3.org/TR/html4/loose.dtd">' . "\n";
    $out .= "<html><head><title>$title</title>\n";
    $out .= "<style type=\"text/css\">li { margin-top: 3px; } </style>\n";
    $out .= "</head><body>\n";
    $out .= "<h1>Why is package X not in testing yet?</h1>\n";
    
    
    if ($name ne "") {
        $out .= "<h2>Checking $name</h2>\n";
        if (0) {
            if (defined $testingversion{$name}) {
                $out .= "<p>Testing version: $testingversion{$name}\n";
            }
            else {
                $out .= "<p>Not available in testing.\n";
            }
            
            if (defined $unstableversion{$name}) {
                $out .= "<br>Unstable version: $unstableversion{$name}\n";
            }
            else {
                $out .= "<br>Not available in unstable.\n";
            }
        }
    
        $debug = 0;
        $depth = 0;
        $broken = 0;
        $firstdep = 1;
        $topname = $name;
    
        $out .= &reason($topname);
        $topnameset = 1;
    
        if (not defined $removing{$name}) {
            if ($newtopname ne "") {
                $out .= &checkbuilddeps($newtopname);
                $out .= &checkolddeps($topname);
                $out .= &checkolddeps($newtopname);
                $out .= &checkconflicts($topname);
                $out .= &checkconflicts($newtopname);
            }
            else {
                $out .= &checkbuilddeps($topname);
                $out .= &checkolddeps($topname);
                $out .= &checkconflicts($topname);
            }
        }
        exit if ($debug);
    
        $topname = $newtopname if ($newtopname ne "");
        $etopname = uri_escape($topname, "+");
        $out .= "<p>[";
        if (!$printalldeps) {
            $out .= "<a href=\"$cginame?package=$etopname;printalldeps=1\">Show all dependencies</a> &middot;\n";
        }
        if ($expand) {
            $out .= "<a href=\"$cginame?package=$etopname\">Show top level only</a> &middot;\n";
        }
        if ($broken) {
            $out .= sprintf "<a href=\"$cginame?package=$etopname;expand=%d\">expand uninstallable packages</a> &middot;\n", $expand + 1;
        }

        $out .= sprintf "<a href=\"$cginame?waiting=$etopname\">pkgs waiting for $topname</a> &middot;\n";
        $out .= sprintf "<a href=\"$cginame?staller=$etopname\">pkgs stalled by $topname</a> &middot;\n";
    
        $out .= "<a href=\"https://packages.qa.debian.org/$etopname\">$topname on packages.qa.d.o</a> &middot;\n";
        $out .= "<a href=\"https://buildd.debian.org/status/package.php?p=$etopname\">$topname's buildd status</a> &middot;\n";
        $out .= "<a href=\"https://bugs.debian.org/cgi-bin/pkgreport.cgi?which=src&amp;data=$etopname&amp;pend-exc=fixed&amp;pend-exc=done\">bugs in $topname</a>";
        $out .= "]\n<hr>";
    }
    
    $n = param("waiting");
    if ($n =~ /(^[a-zA-Z0-9\-\.]+)/) { # ignore silly characters
        my $name = $1;
        if (scalar @{$waits{$name}}) {
            $out .= "<h2>Packages waiting for $name:</h2>\n";
        
            $out .= "<ol>\n";
            for (@{$waits{$name}}) {
                $out .= sprintf("<li>%s (%d days old) is waiting for %s:\n",
                                makelink($_), $age{$_}, makelink($name));
            }
            $out .= "</ol>\n";
        }
        else {
            $out .= "<p>No packages are waiting for $name\n";
        }
    }
    
    $n = param("staller");
    if ($n =~ /(^[a-zA-Z0-9\-\.]+)/) { # ignore silly characters
        my $name = $1;
        $count = 0;
        my $stalls = &recur($name,1);
        if ($stalls ne "\n<ul>\n</ul>\n") {
            $out .= "<h2>Packages stalled by $name:</h2>\n";
            $out .= "<p><i>(Showing only packages which are \"ready\", i.e. past their waiting period.</i>)\n" if ($only_ready);
            $out .= "<p><i>(Showing only packages which already have a version in testing -- no new packages.)</i>\n" if ($only_testing);
            $out .= $stalls;
            %done = ();
        }
        else {
            $out .= "<p>No packages are stalled by $name\n";
        }
    }
    
    $n = param("waitingonly");
    if ($n =~ /(^[a-zA-Z0-9\-\.]+)/) { # ignore silly characters
        my $name = $1;
        if (scalar @{$waits{$name}}) {
            $out .= "<h2>Packages only waiting for $name (no other reasons):</h2>\n";
            $out .= "<ol>\n";
            for (sort @{$waits{$name}}) {
           	if ($depends{$_} == 1 && not defined $minage{$_} &&
                    not defined $bugs{$_} && not defined $outofdate{$_} &&
                    not defined $unsatis{$_} && not defined $breaks{$_} )
           	{
                    $out .= sprintf("<li>%s is only waiting for %s\n",
                                    makelink($_), makelink($name));
           	}
            }
            $out .= "</ol>\n";
        }
        else {
            $out .= "<p>No packages are only waiting for $name\n";
        }

    }
    
    #$out .= "<p>Note: Testing (Sarge) is currently <a href=https://lists.debian.org/debian-devel-announce/2005/05/msg00001.html>frozen</a> and thus no packages go in without hand-approval.\n";
    
    #$out .= "<form action=$cginame>\n";
    #$out .= "<p>Package name: <input type=text name=\"package\">\n";
    #$out .= "<input type=submit></form>";

    $out .= "<p>Search functionality is no longer available.</p>\n";
    $out .= "<p>It may be reintroduced at some point, but this is not yet certain.<p>\n";

    if (scalar keys %version) {
        $out .= sprintf "<p>%d packages are trying to enter testing.\n",
        (scalar (keys %version)) - (scalar (keys %removing));
        $out .= sprintf "%d packages are trying to be removed.\n",
        scalar (keys %removal) if scalar (keys %removal);
    
        my $inout = scalar @inoutpkgs;
        $out .= "$inout packages are <a href=accepted.html>going in/out today</a>.\n";
    }

    $out .= "<br><a href=toplist.html>dependency toplist</a> &middot;\n";
    $out .= "<a href=oldest.html>oldest candidates</a> &middot;\n";
    $out .= "<a href=points.html>highscore</a> &middot;\n";
    $out .= "<a href=stalls.html>top stalls</a> \n";
    $out .= "(<a href=stalls-nonew.html>no new</a>, \n";
    $out .= "<a href=stalls-ready.html>only ready</a>) &middot;\n";
    
    $out .= "<a href=conflicts.html>versioned conflicts</a> &middot;\n";
    $out .= "<a href=olddeps.html>old dependencies</a>\n";
    $out .= "&middot; <a href=obsolete.html>obsolete binaries</a>\n";

    if ($broken) {
        $out .= "<hr><h2>Explanations:</h2>\n";
        $out .= "<p>1. Sometimes packages have seemingly recursive dependencies (adding X makes Y uninstallable, Y is waiting for X). This means the new version of X will break the old version of Y, but there's also a new version of Y that needs the new version of X. As soon as all other dependencies are solved, the two packages can be hinted to go in together.\n";
        $out .= "<p>2. Platforms are tested in alphabetical order, but only the first that breaks is displayed. That's why many packages are reported as uninstallable on alpha. Actually they are most likely uninstallable on many other platforms too, but only the result for alpha is displayed.\n";
    }
    
    $out .= "<p><small><a href=\"mailto:bjorn\@haxx.se\">originally written by Bj�rn Stenberg</a> &middot; <a href=testing.txt>script source code</a></small>\n";
    
    $out .= "<p><small><i>Data is updated hourly.<br>Latest update: $lastmodified</i></small>\n";
    $out .= "</body></html>\n";

    return $out;
}
    
sub debug {
    my $string = shift @_;
    print "",$string if ($debug);
}
    
sub reason {
    my $binary_name = shift @_;
    my $output;

    my $name = &getprovider($binary_name);

    return if (length $name < 2);

    if (!$depth and !$bdepth and ($name ne $binary_name) and !$topnameset) {
        $newtopname = $name;
    }

    return if ($depth and $binary_name eq $topname);

    my $ename = uri_escape($name, "+");
    my $link = makelink($name);

    $rpkg[$depth] = $name;

    if ($filename{$binary_name} ne "" and
        not defined $issourcepackage{$binary_name} and
        not defined $srcforbinary{$binary_name})
    {
        $output .= sprintf("<ul><li>binary package %s, previously provided by %s, is now obsolete</ul>\n",
                           makelink($binary_name),
                           makelink($name));
        return $output;
    }

    if ($name ne $binary_name) {
        $output .= sprintf("<ul><li>binary package %s is part of source package %s\n",
                           makelink($binary_name),
                           makelink($name));
    }

    if (defined $explained{$name} and $alt eq "") {
        if ($name eq $binary_name) {
            $output .= "\n<ul><li>$link is explained above</ul>\n" if ($depth < 3);
            return $output;
        }
        elsif (defined $explained{$name}) {
            $output .= " (explained above)" if ($depth < 3);
            $output .= "</ul>\n";
            return $output;
        }
    }

    $output .= "\n<ul>\n";
    $depth++;

    if ($depth > 10) {
        $output .= "<p>Too deep recursion (10 levels). Aborting.\n";
        return;
    }

    $explained{$name} = 1;
    #print " (this explains $name)";

    my $action;
    my $stop = 0;
    my $exp = 0;
                    
    $reason = 0;
    if (($depth == 1) and
        (not defined $removing{$name}) and
        ($oldversion{$name} eq "-")) {
        $output .= "<li>$link has no old version in testing (trying to add, not update)\n";
    }
    elsif ( $depth == 1 and
            $bdepth == 0 and
            defined $testingversion{$name} and 
            defined $unstableversion{$name} and
            not defined $removing{$name} and
            $testingversion{$name} ne $unstableversion{$name} ) {
        $output .= "<li>trying to update $link from $testingversion{$name} to $unstableversion{$name}\n";
        $output .= "(candidate is $age{$name} days old)\n" if (defined $age{$name});
    }
    if (defined $removing{$name}) {
        $output .= "<li>$link is scheduled for removal\n";
        if ( defined $waits{$name} ) {
            $reason = 1;
            $output .= "<li>$link can't be removed while other packages depend on it:\n";
            $output .= printlinkarray(sort @{$waits{$name}});
        }
    }

    if (defined $newbin{$name}) {
        $reason = 1;
        $output .= "<li>$link is adding $newbin{$name} binaries (no new version)\n";
    }

    if ( not defined $status{$name} ) {
        $reason = 1;
        if (not defined $unstableversion{$name} and
            not defined $testingversion{$name}) {
            if (defined $experimentalversion{$name}) {
                $output .= "<li>$name <a href=\"https://packages.qa.debian.org/$name\"> is only in experimental</a> (no version in testing or unstable)\n";
                $exp = 1;
            }
            else {
                $output .= "<li>$name <a href=\"https://packages.qa.debian.org/$name\">is not available in Debian</a>\n";
            }
        }
        elsif (!&vercmp($unstableversion{$name},$testingversion{$name})) {
            $output .= sprintf "<li>$name <a href=\"https://packages.qa.debian.org/$name\">has the same version in unstable and testing</a> (%s)\n", $testingversion{$name};
            $reason = 1;
        }
        elsif (not defined $unstableversion{$name}) {
            $output .= "<li>$name <a href=\"https://packages.qa.debian.org/$name\"> is only in testing</a> (no unstable version)\n";
        }
        else {
            $output .= "<li>$name <a href=\"https://packages.qa.debian.org/$name\"> is only in unstable</a> (no testing version)\n";
        }
    }
    
    if ( defined $minage{$name} ) {
        $reason = 1;
        $output .= "<li>$link is only $age{$name} days old. It must be $minage{$name} days old to go in.\n";
    }

    if ($filename{$name} =~ /\.udeb/ and
        $email{$name} =~ /debian-boot\@/)
    {
        $reason = 1;
        $output .= "<li>$name is an .udeb from the Debian Installer Team, which are not automatically propagated to testing at this time.";
    }
    
    if ( defined $bugs{$name} and not defined $removing{$name}) {
        $reason = 1;
        $output .= sprintf "<li>$link has <a href=\"https://bugs.debian.org/cgi-bin/pkgreport.cgi?which=src&amp;data=$ename&amp;archive=no&amp;pend-exc=fixed&amp;pend-exc=done&amp;sev-inc=critical&amp;sev-inc=grave&amp;sev-inc=serious\">release-critical bugs</a>\n";
    }

    if (defined $outofdate{$name}) {
        $reason = 1;
        for ( @{$outofdate{$name}} ) {
            $output .= "<li>$link $_\n";
        }
    }
    if (defined $unsatis{$name}) {
        $output .= sprintf("<li>$link has <a href=\"$excuses#$ename\">unsatisfiable dependencies</a>:\n<ul type=circle>\n");
        for my $u_bin (sort keys %{$unsatis{$name}}) {
            for my $u_needs (sort keys %{$unsatis{$name}{$u_bin}}) {
                $output .= sprintf("<li>%s needs %s compiled for %s\n",
                                   makelink($u_bin), makelink($u_needs),
                       join ', ', sort keys %{$unsatis{$name}{$u_bin}{$u_needs}});
            }
        }
        $output .= "</ul>\n";
    }

    if (defined $anyway{$name}) {
        $reason = 1;
#        $output .= "<li>$link should be ignored, but is considered anyway (manual override)\n";
        $output .= "<li>$link should be ignored, $anyway{$name}\n";
    }

#    $output .= sprintf "<li>$link has versions %s/%s\n", $testingversion{$name}, $unstableversion{$name};

    if (not $reason and
        not defined $removing{$name} and
        defined $unstableversion{$name} and
        defined $testingversion{$name} and
#        !&vercmp($unstableversion{$name},$testingversion{$name}))
        $unstableversion{$name} eq $testingversion{$name})

    {
        $reason = 1;
        $output .= sprintf "<li>$name <a href=\"https://packages.qa.debian.org/$name\">has the same version in unstable and testing</a> (%s)\n", $testingversion{$name};

        $stop = 1;
    }

    if (!$stop and (defined $accepted{$name} or defined $final{$name})) {
        $reason = 1;
        if (defined $removing{$name}) {
            $output .= "<li>$link is being removed today\n";
        }
        else {
            $output .= "<li><font color=$green>$link is going in today</font>\n";
        }            

        #if (defined $finalhint{$name}) {
        if (defined $hintby{$name}) {
            $output .= "(thanks to manual hinting by $finalhint{$name})\n";
        }
        $stop = 1;
    }

    if (!$stop and defined $removals{$name}) {
        $output .= "<li><font color=$green>info:$link is requested to be <a href=\"https://bugs.debian.org/$removals{$name}\">removed</a></font>\n";
    }

    if (!$stop and defined $depends{$name} and not defined $removing{$name}) {
        $output .= "<li>$link is waiting for ";
        $output .= &printlinkarray((sort keys %{$deplist{$name}}));

        for ( sort keys %{$deplist{$name}} ) {
            if ($_ ne $topname) {
                $output .= &reason($_);
                $reason = 1;
            }
            last if ($depth > 10);
        }
    }

    if (!$stop and defined $frozen{$name}) {
        $output .= "<li>$link is in freeze; contact debian-release if update is needed\n";
        $reason = 1;
    }

#    if (!$reason and defined $breaks{$name}) {
    if (!$stop and defined $breaks{$name}) {
        $reason = 1;
        $broken++;
        $action = "Adding";
        if ( defined $removal{$name} ) {
            $action = "Removing";
        }
        elsif ( $oldversion{$name} ne "-" ) {
            $action = "Updating";
        }
        if (defined $reportedbreak{$name}) {
            $output .= "<li>$action $link makes $breaks{$name} packages uninstallable (<a href=\"#$name\">see above</a>)\n";
        }
        else {
            my ($lst, $lst2, $arch);
            my $bcount = 0;
            my $dcount = 0;
            my @alist;
            my @blist;
            if ($broken{$name} =~ /^(.+?): (.*)/) {
                $arch = $1;
                my @a;
                my @b;
                my %done;
                for ( split ", ", $2 ) {
                    my $pkg = $_;
                    my $sp = getprovider($pkg);
                    #$pkg = $sp if ($sp ne $pkg);

                    next if (defined $done{$pkg});

                    if (1 and defined $deplist{$sp}{$name})
                    {
                        push @b, &makelink($pkg);
                        push @blist, $pkg;
                        $dcount++;
                    }
                    else {
                        push @a, &makelink($pkg);
                        push @alist, $pkg;
                        $bcount++;
                        #$output .= "--$pkg--";
                    }
                    $done{$pkg} = 1;
                }
                $lst = join ",\n", @a;
                $lst2 = join ",\n", @b;
            }

            # uninstallable due to depends
            $output .= "<li>$action $link makes $dcount depending packages uninstallable on $arch: $lst2\n" if ($dcount);
            if ($expand and ($recursed < $expand)) {
                $recursed++;
                my %done;
                for my $pkg ( @blist ) {
                    next if (defined $done{$pkg});
                    $done{$pkg}=1;
                    $output .= &reason($pkg);
                    last if ($depth > 10);
                }
                $recursed--;
            }

            # other reasons
            $output .= "<a name=\"$name\"></a><li>$action $link makes $bcount non-depending packages uninstallable on $arch: $lst\n" if ($bcount);

            if ( defined $triedrecur{$name}) {
                $output .= " (recur was tried but failed)\n";
            }

            $reportedbreak{$name} = 1;

            if ($expand and ($recursed < $expand)) {
                $recursed++;
                my %done;
                for my $pkg ( @alist ) {
                    next if (defined $done{$pkg});
                    $done{$pkg}=1;
                    $output .= &reason($pkg);
                    last if ($depth > 10);
                }
                $recursed--;
            }


        }
    }

    if (0 and !$stop and not defined $removing{$name}) {
        $output .= &checkbuilddeps($name);
        $output .= &checkolddeps($name);
        $output .= &checkconflicts($name);
    }

    if (!$reason) {
        my $action = "go in";
        $action = "be removed" if (defined $removing{$name});
        if ( @{$comments{$name}}) {
            for ( @{$comments{$name}} ) {
                s|(request to \w+ package by (\w+))|<a href=\"https://release.debian.org/britney/hints/$2\">$1</a>|;
                s|(\w+ request by ([\w-]+))|<a href=\"https://release.debian.org/britney/hints/$2\">$1</a>|;
                s|(requested by ([\w-]+))|<a href=\"https://release.debian.org/britney/hints/$2\">$1</a>|;
                $output .= "<li>$_\n";
            }
        }
        else {
            $output .= "<li>I don't know why $link can't $action. <a href=\"$excuses#$name\">Look here for details</a>.\n";
        }
    }

    if (!$exp and defined $experimentalversion{$name}) {
        $output .= sprintf "<li><font color=$green>info: $name <a href=\"https://packages.qa.debian.org/$name\">has a version in experimental</a> (%s)</font>\n", $experimentalversion{$name};
    }

    $output .= "</ul>\n";
    $output .= "\n</ul>\n" if ($name ne $binary_name);
    $depth--;

#    if (!$depth and $status{$name} eq "Not considered") {
#        print "<p>Note: $link was not test-installed due to the above problems. Test-installing may reveal problems not yet shown here.\n";
#    }
    return $output;
}

sub getprovider {
    my $pkg = shift @_;

    if (defined $issourcepackage{$pkg}) {
        $spkg = $pkg;
    }
    elsif (defined $srcpackage{$pkg}) {
        $spkg = $srcpackage{$pkg};
    }
    elsif (defined $provider{$pkg}) {
        $spkg = $provider{$pkg};
        if (defined $srcpackage{$spkg}) {
            $spkg = $srcpackage{$spkg};
        }
    } 
    elsif (defined $testingprovider{$pkg}) {
        $spkg = $testingprovider{$pkg};
        if (defined $srcpackage{$spkg}) {
            $spkg = $srcpackage{$spkg};
        }
    }
    else {
        $spkg = $pkg;
    }

    return $spkg;
}

sub vercmp {
    my ($ver1, $ver2) = @_;

    return 1 if ($ver1 eq "" or $ver2 eq "");

    return $_system->versioning->compare($ver1, $ver2);

#    # strip out debian version "-n"
#    $ver1 =~ /(\d+:|)([\w\.+-:~]+?)(-[\w+.]+|)$/;
#    my $epoch1 = $1;
#    $ver1 = $2;
#    my $debver1 = $3;
#
#    $ver2 =~ /(\d+:|)([\w\.+-:~]+?)(-[\w+.]+|)$/;
#    my $epoch2 = $1;
#    $ver2 = $2;
#    my $debver2 = $3;
#
#    return 1 if ($epoch1 > $epoch2);
#    return -1 if ($epoch2 > $epoch1);
#
#    my @v1 = split /\b/, $ver1;
#    my @v2 = split /\b/, $ver2;
#
#    my $num = scalar @v1;
#    $num = scalar @v2 if (scalar @v2 > $num);
#
#    for (0 .. $num) {
#        $a = $v1[$_];
#        $b = $v2[$_];
#        if ($a =~ /^\d/ or $b =~ /^\d/) {
#            # numerical comparison
#            return 1 if ($a > $b);
#            return -1 if ($a < $b);
#        }
#        else {
#            # lexical comparison
#            while (ord $a == ord $b) {
#                $a = substr($a, 1);
#                $b = substr($b, 1);
#                last if (!length $a or !length $b);
#
#                if (($a > 0) or ($b > 0)) {
#                    # number in string: use numerical comparison
#                    return 1 if ($a > $b);
#                    return -1 if ($a < $b);
#                    last;
#                }
#            }
#
#            return 1 if (ord $a > ord $b);
#            return -1 if (ord $a < ord $b);
#        }
#    }
#
#    return 0 if ($debver1 eq $debver2);
#    return vercmp($debver1, $debver2);
}

sub cmpverstring {
    my ($ver,$tver) = @_;
    my $yes = 0;

    if ($ver =~ />> *(.+)/) {
        $yes = 1 if (&vercmp($tver,$1) > 0);
    }
    elsif ($ver =~ /<< *(.+)/) {
        $yes = 1 if (&vercmp($tver,$1) < 0);
    }
    elsif ($ver =~ />=? *(.+)/) {
        $yes = 1 if (&vercmp($tver,$1) >= 0);
    }
    elsif ($ver =~ /<=? *(.+)/) {
        $yes = 1 if (&vercmp($tver,$1) <= 0);
    }
    elsif ($ver =~ /= *(.+)/) {
        $yes = 1 if (&vercmp($tver,$1) == 0);
    }
    elsif ($ver =~ /0$/) {
        $yes = 1 if ($tver ne "");
    }
    else {
        $output .= "<p>Failed parsing version string: $ver / $tver\n"; 
    }
    return $yes;
}

sub checkbuilddeps {
    my $binary_name = shift @_;
    my $name = $binary_name;
    my $output;
    my $print = 0;

    if (defined($srcpackage{$binary_name})) {
        $name = $srcpackage{$binary_name};
    }

    $bdepth++;
    if ($bdepth > 40) {
        warn "Too recursive in pkg $topname";
        return;
    }
    $stack[$bdepth] = $name;

    debug "entering checkbuilddeps($name)\n";

    return if (defined $bexplained{$name});
    $bexplained{$name} = 1;

    if (!$depsaid) {
        $output .= "<h3>Dependency analysis (including build-depends; i386 only):</h3>\n";
        $depsaid = 1;
    }
    $output .= "<ul>\n" if (!$depth);

    for my $dep (sort keys %{$builddeps{$name}}) {
        my $str = &tryalternatives($name, $dep, \&_checkbuilddeps);

        if ($str ne "") {
            $print = 1;
            $output .= $str;
        }
        else {
            delete $explained{$name};
        }
    }

    $output .= "</ul>\n" if (!$depth);
    $bdepth--;

    return $output if ($print);
    return "";
}

sub _checkbuilddeps {
    my ($name, $pkg, $ver) = @_;

    my $print = 0;
    my $output;

    $bdepth = 0;

    &debug("_checkbuilddeps($name, $pkg, $ver)\n") if ($debug);

    # remove surrounding spaces
    if ($pkg =~ /^\s*(.+?)\s*$/) {
        $pkg = $1;
    }

    my $spkg = &getprovider($pkg);
    return "" if ($spkg eq $name);

    my $link = makelink($name);
    my $str = "<li>$alt$link depends on " . makelink($pkg);
    $str .= " $ver" if ($ver ne "0");

    my $tver = $testingversion{$pkg};
    if (0==&cmpverstring("$pkg $ver", $tver)) {
        $output .= "$str";
        if ($tver) {
            $print = 1;
            $output .= " but testing has $tver (unstable has $unstableversion{$pkg})\n";
            $output .= &reason($pkg);
            $output .= &checkbuilddeps($pkg);
        }
        else {
            if (defined $providerlist{$pkg}) {
                $output .= ", provided by: " . &printlinkarray(@{$providerlist{$pkg}});
                my $options = scalar @{$providerlist{$pkg}};
                my $count = 0;
                $output .= "<ul>\n";

                foreach (@{$providerlist{$pkg}}) {
                    my $link = makelink($_);
                    if (defined $testingversion{$_}) {
                        $count++;
                        $output .= "<ul><font color=$green><li>info: $link has version $testingversion{$_} in testing</font></ul>\n";
                    }
                    else {
                        $output .= "<li>$link is not available in testing:\n\n";
                        $output .= &reason($_);
                        $output .= &checkbuilddeps($_);
                        #$output .= "</ul>\n";
                    }
                }
                $output .= "</ul>\n";
                $print = 1 if ($count == 0 or $printalldeps);
            }
            else {
                $print = 1;
                $output .= " which is not available in testing\n";
                $output .= &reason($pkg);
                $output .= &checkbuilddeps($pkg);
            }
        }
    }
    elsif (not defined $testingversion{$pkg} and
           not defined $testingprovider{$pkg}) {
        $output .= "$str, which is not available in testing";
        $output .= &reason($pkg);
        $output .= &checkbuilddeps($pkg);
        $print = 1;
    }
    
    if ($printalldeps and !$print) {
        my $ver = $builddeps{$name}{$pkg};
        my $tver = $testingversion{$pkg};
        if (!$testingversion{$pkg} and defined $testingprovider{$pkg}) {
            $tver = $testingversion{$testingprovider{$pkg}};
        }
        $str =~ s/<li>/<li>info: /;
        $output .= sprintf "<font color=$green>$str %s (ok, testing has version $tver)</font>\n", $ver ? $ver : "";
        $print = 1;
    }

    return $output if ($print);
    return "";
}

sub checkconflicts {
    my $pkg = shift @_;
    my $str;
    my $output;

    &debug("checkconflicts($pkg)\n") if ($debug);

    for my $conf (sort keys %{$conflicts{$pkg}}) {
        my $s = &tryalternatives($pkg, $conf, \&_checkconflicts);
        if ($s ne "" or $printalldeps) {
            $output .= $s;
        }
    }
    if (1 and $output ne "") {
        $output = "<h3>Package version conflicts:</h3>\n<ul>\n$output</ul>\n";
    }

    return $output;
}

sub _checkconflicts {
    my ($name, $pkg, $ver) = @_;

    debug "_checkconflicts($name, $pkg, $ver)\n";
    return "" if ($ver eq "0" or not defined $testingversion{$pkg});

    my $spkg = &getprovider($pkg);
    return "" if ($spkg eq $name);

    if (&cmpverstring("$pkg $ver", $testingversion{$pkg})) {
        return "" unless (defined $deplist{$name}{$pkg});
        return sprintf("<li>%s conflicts with %s $ver but testing has $testingversion{$pkg}\n",
                       makelink($name), makelink($pkg));
    }
    return "";
}

sub checkolddeps {
    my $pkg = shift @_;
    my $output;

    debug "checkolddeps($pkg): " . (scalar keys %{$revdeps{$pkg}}) . " revdeps\n";

    for my $dep (sort keys %{$revdeps{$pkg}}) {
        debug "$pkg => $dep\n";
        for my $dep2 (sort keys %{$builddeps{$dep}}) {
            debug "$pkg => $dep => $dep2\n";
            next unless (index($dep2, $pkg) == 0);
            my $str = &tryalternatives($dep, $dep2, \&_checkolddeps);
            if ($str ne "" or $printalldeps) {
                $output .= $str;
            }
        }
    }
    if ($output ne "") {
        $output = "<h3>Dependencies on old versions of $pkg:</h3>\n<ul>\n$output</ul>\n";
    }
    return $output;
}

sub _checkolddeps {
    my ($name, $pkg, $ver) = @_;

    debug "_checkolddeps($name, $pkg, $ver)\n";
    return "" if ($ver =~ />/);
    return "" if ($ver eq "0" or not defined $unstableversion{$pkg});

    my $spkg = &getprovider($name);
    return "" if ($spkg eq $pkg);

    if (not &cmpverstring("$pkg $ver", $unstableversion{$pkg})) {
        return sprintf("<li>%s depends on %s $ver, but $unstableversion{$pkg} is going in\n",
                       makelink($name), makelink($pkg));
    }
    return "";
}

# split a conditional depend/conflict and check each one
sub tryalternatives {
    my ($name, $arg, $function) = @_;
    my $print = 0;
    my $count = 0;
    my $loop = 0;

    my $output;
    debug "tryalternatives($name, $arg)\n";

    my @alternatives = split / *\| */, $arg;
    my $options = scalar @alternatives;

    for my $p (@alternatives) {
        my $v = 0;
        my $s;
        $loop++;

        # Ignore Build-Profiles
        $p =~ s/\s*[<][^>]+[>]\s*//g;

        # split package and version
        if ($p =~ /(.+?)\s*\((.+?)\)/) {
            $p = $1;
            $v = $2;
        }
        # remove arch
        if ($p =~ /(.+?)\s*\[(.+?)\]/) {
#            next; # don't try to parse arch-specific dependencies
            $p = $1;
        }

        if (scalar @alternatives > 1) {
            $alt = sprintf "alternative %d/%d: ", $loop, scalar @alternatives;
        }
        else {
            $alt = "";
        }

        $s = &$function($name, $p, $v);
        if ($s ne "") {
            $count++;
            $output .= $s;
            #debug "alternative $count/$options failed: $s\n";
        }
    }
    if ($count == $options) {
        if ($options > 1) {
            if ($printalldeps) {
                $output = sprintf "<p><li><b>%d alternatives:</b> $arg\n<ul>$output</ul>", scalar @alternatives;
            }
            else {
                $output = sprintf "<p><li>All %d alternatives failed: $arg\n<ul>$output</ul>", scalar @alternatives;
            }
        }
        return $output;
    }
    return "";
}

sub makelink {
    my $name = shift @_;
    return sprintf("<a href=\"$cginame?package=%s\">$name</a>",
                   uri_escape $name, "+");
}

sub printbreaklist
{
    my ($list,$name) = @_;
    my @a;
    for ( split ", ", $list ) {
        next if (defined $deplist{$_}{$name});
        next if (defined $deplist{$srcpackage{$_}}{$name});
        #next if ($srcpackage{$_} ne $_);
        push @a, &makelink($_);
        #push @a, "($srcpackage{$_})" if (defined $srcpackage{$_});
    }
    return join ",\n", @a;
}

sub printlinkarray
{
    my @list = @_;
    my @a;
    for (  @list ) {
        push @a, &makelink($_);
    }
    return join ",\n", @a;
}

sub recur
{
    my ($name,$print) = @_;
    $done{$name} = 1;
    my $output;

    $output .= "\n<ul>\n" if ( $print );

    for ( @{$waits{$name}} ) {
        next if (defined $done{$_});
        next if ($only_testing and not defined $testingversion{$_});
        next if ($only_ready and $age{$_} < $minage{$_});
        if (defined $installations{$_}) {
            $score += $installations{$_};
            $l .= "$_:$installations{$_},";
        }
        $count ++;
        if ( $print ) {
            my $days;
            if ($minage{$_}) {
                $days = sprintf("(%d day%s of %s)",
                                $age{$_},
                                $age{$_} > 1 ? "s" : "",
                                $minage{$_});
            }
            else {
                $days = sprintf "(%d days old)", $age{$_};
            }
            $output .= sprintf("<li>$count: %s $days waits for $name\n",
                               &makelink($_));
        }
        $output .= &recur($_,$print) unless (defined $done{$_});
    }
    $output .= "</ul>\n" if ( $print );

    return $output;
}

sub readfiles {
    my $readvotes = shift @_;

    #
    # Reading the Packages files must happen before the update output
    # is read.
    #

    # read data about packages in unstable distribution
    for (("srcs", "pkgs")) {
        my $srcs = 0;
        $srcs = 1 if ($_ eq "srcs");

        open PACKAGES, "<$_" or die("Cannot open $_: $!");
        for (<PACKAGES>) {
            if (/^Package: (.*)$/) {
                $source = $1;
                $issourcepackage{$1} = 1 if ($srcs);
            }

            # skip repeated package entries
            # (this happens while non-US is not updated. and since "nonus" is 
            # alphabetically sorted last, they will appear last in the list...)
            next if (defined $filename{$source});

            if (/^Filename: (.*)$/) {
                $filename{$source} = $1;
            }
            elsif (/^Binary: (.*)$/) {
                my @pkgs = split(/[ ,]+/, $1);
                foreach $pkg (@pkgs) {
                    $srcpackage{$pkg} = $source;
                    $srcforbinary{$pkg} = $source;
                }
            }
            elsif (/^Version: (.*)$/) {
                $unstableversion{$source} = $1;
            }
            elsif (/^Directory: (.*)$/) {
                $directory{$source} = $1;
            }
            elsif (/^Source: (.+)$/) {
                my $tmp = $1;
                if ($tmp =~ /^(.+?) \(.+\)/) {
                    $tmp = $1;
                }
                $srcpackage{$source} = $1;
            }
            elsif (/^Provides: (.*)$/) {
                for $pkg (split(/[ ,]+/, $1)) {
                    $provider{$pkg} = $source;
                    push @{$providerlist{$pkg}}, $source;
                }
            }
            elsif (/^Conflicts: (.*)$/) {
                for $pkg (split(/ *, */, $1)) {
                    $conflicts{$source}{$pkg} = 1;
                }
            }
            elsif (/^Maintainer: (.*?) <(.+?)>$/) {
                $maintainer{$source} = $1;
                $email{$source} = $2;
            }
    #        elsif (/^.*Depends.*: (.*)$/) {
            elsif (/^.*Depends: (.*)$/) {
    #        elsif (/^Depends: (.*)$/) {
                foreach my $dep (split(/ *, */, $1)) {
                    # save entire dependecy, versions alternatives and all
                    $builddeps{$source}{$dep} = 0;
    
                    # remember dependencies on older packages
                    if ($dep =~ /([^ ]+?) *\( *([<=].+?)\)/) {
                        $olddeps{$1}{$source} = $2;
                    }
    
                    # remember reverse dependencies
                    for my $s (split / *\| */, $dep) {
                        if ($s =~ /^([^ ]+?) *\([<>=]+.+?\)/) {
                            $revdeps{$1}{$source} = 1;
                        }
                        else {
                            $revdeps{$s}{$source} = 1;
                        }
                    }
                }
            }
        }
        close PACKAGES;
    }
    
    # read data about packages in testing distribution
    for (("testing.pkgs", "testing.srcs")) {
        open PACKAGES, "<$_" or die("Cannot open $_: $!");
        for (<PACKAGES>) {
            if (/^Package: (.*)$/) {
                $source = $1;
            }
            elsif (/^Version: (.*)$/) {
                $testingversion{$source} = $1;
            }
            elsif (/^Provides: (.*)$/) {
                for $pkg (split(/[ ,]+/, $1)) {
                    $testingprovider{$pkg} = $source;
                }
            }
        }
        close PACKAGES;
    }

    # read data about packages in experimental distribution
    for (("experimental.pkgs", "experimental.srcs")) {
        open PACKAGES, "<$_" or die("Cannot open $_: $!");
        for (<PACKAGES>) {
            if (/^Package: (.*)$/) {
                $source = $1;
            }
            elsif (/^Version: (.*)$/) {
                $experimentalversion{$source} = $1;
            }
            elsif (/^Provides: (.*)$/) {
                for $pkg (split(/[ ,]+/, $1)) {
                    $experimentalprovider{$pkg} = $source;
                }
            }
        }
        close PACKAGES;
    }

    my $name;
    open EXCUSES, "<$BASE_DIR/update_excuses.html" or die("Cannot open update_excuses.html: $!");
    for ( <EXCUSES> ) {
        if ( /<a id=\"([^\/]+?)(\/([^\"]+))?\".*?\((.+?) to (.+?)\)/ ) {
            $name = $1;
            $arch = $3;
            my $oldv = $4;
            my $newv = $5;
            if ( $name =~ /^-(.+)/ ) {
                $name = $1;
                $removing{$name} = 1;
            } 
            $oldversion{$name} = $oldv;
            $version{$name} = $newv;
       }
        elsif ( /^<li>(\d+) days old/ ) {
            $age{$name} = $1;
        }
        elsif ( /^<li>Too young, only (\d+) of (\d+)/ ) {
            $age{$name} = $1;
            $minage{$name} = $2;
        }
        elsif ( /^<li>(Not considered)/ ) {
            $status{$name} = $1;
        }
        elsif ( /^<li>(Valid candidate)/ ) {
            $status{$name} = $1;
        }
        elsif ( /^<li>New binary:/ ) {
            $newbin{$name} = "$arch";
        }
        elsif ( /^<li>Trying to remove package/ ) {
            $removing{$name} = 1;
        }
        elsif ( /^<li>Should ignore, (.*)/ ) {
            $anyway{$name} = $1;
        }
        elsif ( m!^<li>(?:out of date|old binaries left) on <a href=\"(.+?)\".+?>(.+?)</a>: (.+)$! ) {
            my ($url,$port,$pkglist) = ($1, $2, $3);
            my @org_pkgs;
            my $ver;
            # The version handling here isn't quite right, as it will list
            # all packages with the version encountered last in the excuse.
            # It'll do for now at least though.
            foreach my $pkginfo (split(/; /, $pkglist)) {
                while ( $pkginfo =~ m!\ ?(.+?) \(from <a href=\".+?\">(.+?)</a>\)(.*)! ) {
                    push @org_pkgs, split(/[ ,]+/, $1);
                    $ver = $2;
                    $pkginfo = $3;
                }
            }
            $url =~ s/\&ver=.*//;
            $url =~ s/&/&amp;/;
            my @pkgs = grep({!defined $incoming{$name}{$version{$name}}{$port}} @org_pkgs);
            my $pnum = scalar @pkgs;
            my $links = sprintf("$pnum binar%s", $pnum > 1 ? "ies" : "y");
            if (scalar @pkgs < 8 and scalar @pkgs) {
                $links .= ": " . &printlinkarray(@pkgs);
            }
            if (defined $incoming{$name}{$version{$name}}{$port}) {
                push @{$outofdate{$name}}, "build for $port is uploaded to <a href=\"http://incoming.debian.org/\">incoming</a>";
            }
            else {
                push @{$outofdate{$name}}, "is <a href=\"$url\">not yet built on $port</a>: $ver vs $version{$name} (missing $links) $comment";
            }

            # Check if the offending binary packages does not exist in the new
            # version and has to be�removed by the ftp master using "dak rm"
            # to make "britney" happy again.
            foreach $pkg (@pkgs) {
                if (not defined $srcforbinary{$pkg} and
                    not defined $notsource{$pkg.$arch})
                {
                    push (@{$outofdate{$name}},
                          sprintf("no longer provides binary %s. ftpmaster needs to <a href=obsolete.html>remove it</a>.",
                                  &makelink($pkg)));
                    $notsource{$pkg.$arch} = 1;
                }
            }
        }
        elsif ( /^<li>.+?buggy.+?\((\d+) .+? (\d+)/) {
            if ( $1 > $2 ) {
                $bugs{$name} = 1;
            }
        }
        elsif ( /^<li>Updating (.+?) introduces new bugs/) {
            $binbugs{$name}{$1} = 1;
        }
        elsif ( /^<li>Not touching package, as requested by freeze/) {
            $frozen{$name} = 1;
        }
    #
    # Unsatisfied dependencies don't affects a packages Valid Candidate status
    #
    #    elsif ( /^<li>(.+?)\/(.+?) unsatisfiable Depends: ([^ ]+)/) {
    #        $a = "";
    #        $a = "<a href=\"$cginame?package=$1\">$1</a> needs ";
    #        $unsatis{$name}{$1}{$3}{$2} = 1;
    #    }
        elsif ( /^<li>Depends: (.+?) .+?>(.+?)</ ) {
            $depends{$name}++;
            $elements{$2}++;
            $deplist{$name}{$2} = 1;
            push @{$waits{$2}}, $name;
        }
        elsif (/^<li>Maintainer/) {
        }
        elsif ( /^<li>(.*)/ ) {
            push @{$comments{$name}}, $1;
        }
    }
    close EXCUSES;
    
    open OUTPUT, "<$BASE_DIR/update_output.txt" or die("Cannot open update_output.txt: $!");
    for ( <OUTPUT> ) {
        if ( /^skipped: (-?)([^ \/]+)(\/([^\"]+))? / ) {
            $name = $2;
            $removal{$name} = 1 if ($1 eq "-");
            $status{$name} = "skipped" if (not defined $status{$name});
        } 
        elsif ( /^endloop:/ ) {
            $name = "";
        } 
        elsif ( /^accepted: (-?)([^ \/]+)(\/([^\"]+))? / ) {
            $name = $2;
            next if ($name =~ /\//); # ignore arch-specific binary updates
            $removal{$name} = 1 if ($1 eq "-");
            $accepted{$name} = 1;
            if ( $recur ne "" ) {
                push @recurred, $2;
            }
        }
        elsif ( /^final: (.+)/ ) {
            for ( split ',', $1 ) {
                my $name = $_;
                if (/^-(.*)/) {
                    $name = $1;
                    $removal{$name} = 1;
                }
                next if ($name =~ /\//); # ignore arch-specific binary updates
                $final{$name} = 1;
                $accepted{$name} = 1;
                if ($lasthinted ne "") {
                    $finalhint{$name} = $lasthinted;
                }
            }
            $lasthinted = "";
            $recur = 0;
        }
        elsif ( /(?:[hH]int|[eE]asy) from (.+?): (.+)/ ) {
            $lasthinted = $1;
            for ( split ' ', $2 ) {
                /(.+?)\//;
                $hintby{$1} = $lasthinted;
    #            print ">>$lasthinted hinted $1\n";
            }
        }
        elsif ( /^recur: \[.*?\] (.+) \d+\/\d+/ ) {
            $recur = $1;
            $triedrecur{$1} = 1;
        }
        elsif ( /^FAILED/ ) {
            #print "<!-- Recur $recur failed, removing accepted: ". join (',', @recurred) . "-->\n";
            for ( @recurred ) {
                delete $accepted{$_};
            }
            $recur = "";
            @recurred = ();
            $lasthinted = "";
        }
    #    elsif ( /^   all: (.+)/ ) {
    #        for ( split ' ', $1 ) {
    #            /(-|)(.*)/;
    #            $accepted{$2} = 1;
    #        }
    #    }
        elsif (/^    \* (.*)/) {
            $count = scalar split ",", $1;
            if (defined $breaks{$name}) {
                # only save the lowest break count
                if ($count < $breaks{$name}) {
                    $breaks{$name} = $count;
                    $broken{$name} = $1;
                }
            }
            else {
                $breaks{$name} = $count;
                $broken{$name} = $1;
            }
    #        print STDERR "$name breaks $count packages\n";
        }
    }
    
    close OUTPUT;

    my @tmplist = (keys %accepted, keys %final);
    my %pcount;
    @inoutpkgs = grep { ++$pcount{$_} < 2 } @tmplist;
    %eek = ();
    for (@inoutpkgs) {
        next if (!&vercmp($unstableversion{$_},$testingversion{$_}));
        $eek{$_} = 1;
    }
    @inoutpkgs = keys %eek;

    # read popcon data? (for package scoring)
    open POPCON, "<by_inst" or die("Cannot open by_inst (popcon data): $!");
    for (<POPCON>) {
        if (/^\d+\s+([^\s]+)\s+(\d+)/) {
            $installations{$1} = $2;
        }
    }
    close POPCON;

    open REMOVALS, "<removals.html" or die("Cannot open removals.html: $!");
    for ( <REMOVALS> ) {
        if ( /Bug #(\d+)\D.+\"https:\/\/packages\.qa\.debian\.org\/(.+?)\"/ ) {
             my $bugnr   = $1;
             my $package = $2;
             $removals{$package} = $bugnr;
         }
    }
    close REMOVALS;

}
    
sub handle_argv
{
    if ($ARGV[0] eq "mkcache") {
        # printf "Making %d cache files\n", scalar keys %unstableversion;
        for (('a'..'z', 0..9)) {
            mkdir("cache/$_");
            #print "Making cache dir $_\n";
        }
        my $i = 0;
        for (keys %unstableversion) {
            /-?((.).+)\/?/;
            my $name = $1;
            my $dir = $2;
            if (1) {
                $expand = 0;
                if (1) {
                    $depsaid = 0;
                    open OUT, ">cache/$dir/$name.html" or die "Failed creating cache/$name.html: $!";
                    print OUT &mkpage($name);
                    close OUT;
                }
                else {
                    &mkpage($name);
                }
#                $expand = 1;
#                open OUT, ">cache/$dir/$name.1" or die "Failed creating cache/$name: $!";
#                print OUT &mkpage($name);
#                close OUT;
            }
            else {
                #print "pkg/$1\n";
            }
            #$i++;
            #print "$i\n" if (($i % 1000) == 0);
        }
        open OUT, ">cache/_index.html" or die "Failed creating cache/_index.html: $!";
        print OUT &mkpage("");
        close OUT;
        exit;
    }
    elsif ($ARGV[0] eq "conflicts") {
        for my $pkg (sort keys %version) {
            for my $conf (sort keys %{$conflicts{$pkg}}) {
                print &tryalternatives($pkg, $conf, \&_checkconflicts);
            }
        }
        exit;
    }
    elsif ($ARGV[0] eq "olddeps") {
        for my $pkg (sort keys %version) {
            for my $dep (sort keys %{$revdeps{$pkg}}) {
                print "$pkg => $dep\n" if ($debug);
                for my $dep2 (sort keys %{$builddeps{$dep}}) {
                    print "$pkg => $dep => $dep2\n" if ($debug);
                    next unless index($dep2, $pkg)==0;
                    print &tryalternatives($dep, $dep2, \&_checkolddeps);
                }
            }
        }
        exit;
    }
    elsif ($ARGV[0] eq "obsolete") {
        for my $pkg (sort keys %outofdate) {
            my @ood = grep /no longer provides/, @{$outofdate{$pkg}};
            for (@ood) {
                my $days;
                if ($minage{$pkg}) {
                    $days = sprintf("(%d day%s of %s)",
                                    $age{$pkg},
                                    $age{$pkg} > 1 ? "s" : "",
                                    $minage{$pkg});
                }
                else {
                    $days = sprintf "(%d days old)", $age{$pkg};
                }

                printf("%s $days $_<br>\n", &makelink($pkg));
            }
        }
        exit;
    }
    elsif ($ARGV[0] eq "accepted") {
        my ($in,$out) = (0,0);
        for (@inoutpkgs) {
            if (defined $removal{$_}) {
                $out++;
            }
            else {
                $in++;
            }
        }
        printf "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n";
        printf "<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.1//EN\" \"http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd\">\n";
        printf "<html xmlns=\"http://www.w3.org/1999/xhtml\">\n";
        printf "<head><title>Packages entering or leaving testing today</title></head>\n";
        printf "<body>\n";
        printf "<h1>Packages entering or leaving testing today</h1>\n";

        printf "<h2>$in packages are going in:</h2>\n";
        printf "<ul>\n";
        for (grep {! defined $removal{$_}} sort @inoutpkgs) {
            s|(/(.+))||;
            my $arch = $2;
            if ($arch ne "") {
                printf("<li>%s is getting updated $arch binaries</li>\n",
                       makelink($_));
            }
            else {
                if (defined $age{$_}) {
                    printf("<li>%s is going in after $age{$_} days%s%s</li>\n",
                           makelink($_),
                           $elements{$_} ? sprintf " (<a href=\"$cginame?waiting=%s\">$elements{$_} packages waiting</a>)", uri_escape($_, "+") : "",
                           defined $hintby{$_} ? " (hinted by $hintby{$_})" : "");
                }
                else {
                    printf("<li>%s is going in</li>\n",
                           makelink($_));
                }
            }
        }
        printf "</ul>\n";

        printf "<h2>$out packages are being removed:</h2>\n";
        printf "<ul>\n";
        for (grep {defined $removal{$_}} sort @inoutpkgs) {
            printf("<li>%s is being removed%s</li>\n",
                   makelink($_),
                   defined $hintby{$_} ? " (hinted by $hintby{$_})" : "");
        }
        printf "</ul>\n";

        printf "</body>\n";
        printf "</html>\n";
        exit;
    }

    # print toplist
    for ( keys %version ) {
        $ename = uri_escape $_, "+";
        $whynot = "";
        @why = ();
        $bad = 0;
        my $accept = 0;
        if (defined $frozen{$_}) {
            push @why, "frozen";
        }

        if (defined $accepted{$_} or defined $final{$_}) {
            if (defined $removing{$_}) {
                push @why, "REMOVED";
            }
            else {
                push @why, "ACCEPTED";
            }
            $accept = 1;
        }
        if (defined $bugs{$_}) {
            
            push @why, "<a href=\"https://bugs.debian.org/cgi-bin/pkgreport.cgi?which=src&amp;data=$ename&amp;archive=no&amp;pend-exc=fixed&amp;pend-exc=done&amp;sev-inc=critical&amp;sev-inc=grave&amp;sev-inc=serious\">has RC bugs</a>";
            $bad++;
        }
        if (defined $binbugs{$_}) {
            my $bin = uri_escape( (keys %{$binbugs{$_}})[0], "+");
            push @why, "<a href=\"https://bugs.debian.org/cgi-bin/pkgreport.cgi?which=src&amp;data=$bin&amp;archive=no&amp;pend-exc=fixed&amp;pend-exc=done&amp;sev-inc=critical&amp;sev-inc=grave&amp;sev-inc=serious\">has RC bugs</a>";
            $bad++;
        }
        if (defined $outofdate{$_}) {
            push @why, "not yet built";
        }
        if (defined $unsatis{$_}) {
            push @why, "unsatisfiable";
            $bad++;
        }
        if (defined $breaks{$_}) {
            push @why, "breaks $breaks{$_} pkgs";
            $bad++;
        }
        if (defined $minage{$_}) {
            next if ($ARGV[0] eq "points" and scalar @why == 0);
            push @why, "too young";
        }
        else {
            $bad++;
        }
        if (defined $depends{$_}) {
            next if ($ARGV[0] eq "points" and scalar @why == 0);
            push @why, "waiting for ". join ', ', sort keys %{$deplist{$_}};
            #push @why,"waiting for ". printlinkarray(keys %{$deplist{$_}});
        }

        if (defined $removals{$_}) {
            push @why, "<a href=\"https://bugs.debian.org/$removals{$_}\">remove requested</a>";
        }

        my $state;
        if (defined $testingversion{$_}) {
            $state = "update";
        }
        else {
            $state = "new pkg";
        }



        $whynot = join ", ", @why;

        #my $maint = "[<a href=\"mailto:$email{$_}\">$maintainer{$_}</a>]" if (defined $maintainer{$_});
        my $maint = "[<a href=\"https://qa.debian.org/developer.php?login=$email{$_}\">$maintainer{$_}</a>]" if (defined $maintainer{$_});
        
        if ($ARGV[0] eq "age") {
            next if ($age{$_} == 0);
            print $age{$_} . " days: " . makelink($_) . " (<a href=\"$cginame?waiting=$ename\">" . ($elements{$_} || "0") .
                    " waiting</a>, $state, $whynot) $maint<br>\n";

        }
        elsif ($ARGV[0] eq "points") {
            $score = 1;
            $l = "";
            %done = ();
            &recur($_,0);
            $score += $installations{$_};
            #$score *= (1 + $age{$_}/100);
            $l .= "$_:$score, ";
            if (defined $bugs{$_}) {
                $score *= 1.5;
                $l .= "RC = *1.5, ";
            }
            elsif (defined $frozen{$_}) {
                next;
            }

            next if (!$bad or (defined $accepted{$_} or defined $final{$_}));

            
            if ($score) {
                print "$score points: " . makelink($_) . " (<a href=\"$cginame?staller=$ename\">" . ($elements{$_} || 0) . " stalled</a>, " .
                        $age{$_} . " days old, $whynot) $maint <!-- $l --> <br>\n";
            }
        }
        elsif ($ARGV[0] eq "stalls") {
            $count = 0;
            %done = ();
            &recur($_,0);
            printf( "%d packages are <a href=\"$cginame?staller=$ename\">stalled</a> by <a href=\"$cginame?package=$ename\">$_</a>%s (%d days old, $whynot) $maint%s<br>\n",
                    $count,
                    $accept ? $startgreen : "",
                    $age{$_}, 
                    $accept ? $stopgreen : "") if ($count);
        }
        elsif ($ARGV[0] eq "stalls-nonew") {
            $count = 0;
            %done = ();
            next if (not defined $testingversion{$_});
            $only_testing = 1;
            &recur($_,0);
            printf( "%d in-testing packages are <a href=\"$cginame?staller=$ename;nonew=1\">stalled</a> by <a href=\"$cginame?package=$ename\">$_</a>%s (%d days old, $whynot) $maint%s<br>\n",
                    $count,
                    $accept ? $startgreen : "",
                    $age{$_}, 
                    $accept ? $stopgreen : "") if ($count);
        }
        elsif ($ARGV[0] eq "stalls-ready") {
            $count = 0;
            %done = ();
            next if ($age{$_} < $minage{$_});
            $only_ready = 1;
            &recur($_,0);
            printf( "%d otherwise ready packages are <a href=\"$cginame?staller=$ename;ready=1\">stalled</a> by <a href=\"$cginame?package=$ename\">$_</a>%s (%d days old, $whynot) $maint%s<br>\n",
                    $count,
                    $accept ? $startgreen : "",
                    $age{$_}, 
                    $accept ? $stopgreen : "") if ($count);
        }
        else {
            printf( "%d packages <a href=\"$cginame?waiting=$ename\">wait</a> for <a href=\"$cginame?package=$ename\">$_</a>%s (%d days old, $whynot) $maint%s<br>\n",
                    $elements{$_},
                    $accept ? $startgreen : "",
                    $age{$_},
                    $accept ? $stopgreen : "") if ($elements{$_});
        }
    }
}

